import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-config-app',
  templateUrl: './config-app.component.html',
  styleUrls: ['./config-app.component.css']
})
export class ConfigAppComponent implements OnInit {

  sub_link_menu:string="Site"

  constructor() { }

  ngOnInit() {
  }

  changeMenu(value :string){
        this.sub_link_menu=value
  }

}
