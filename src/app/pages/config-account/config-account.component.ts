import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-config-account',
  templateUrl: './config-account.component.html',
  styleUrls: ['./config-account.component.css']
})
export class ConfigAccountComponent implements OnInit {

  sub_link_menu:string="Compte utilisateur"

  constructor() { }

  ngOnInit() {
  }

  changeMenu(value :string){
        this.sub_link_menu=value
  }

}
