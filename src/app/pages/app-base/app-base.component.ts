import { Component, OnInit } from '@angular/core';
import { MenuGrantedService } from 'src/app/services/menu.granted.service';

@Component({
  selector: 'app-app-base',
  templateUrl: './app-base.component.html',
  styleUrls: ['./app-base.component.css']
})
export class AppBaseComponent implements OnInit {
  
  profil:number=0;
  constructor (private servMenu:MenuGrantedService){}
  ngOnInit(): void {
    this.getProfilUser()
  }

  getProfilUser():void{
    this.profil= this.servMenu.getProfilUser()
    console.log(this.profil)
  }
  isCollapsed = false;

  toggleCollapsed(): void {
    this.isCollapsed = !this.isCollapsed;
  }

}
