import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { Localstore } from 'src/app/store/local.store';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  validateForm!: FormGroup;

  progress=false
  statusEnable=false;
  errorMessage: string="";
  connect=true;
  user:User | undefined;

 
  constructor(private fb: FormBuilder,private authService: AuthService,private router:Router) {}

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      email: [null, [Validators.required]],
      password: [null, [Validators.required]],
    });
  }


  // Authentification
  login(myform : any){
    this.progress=true
    this.statusEnable=true
    this.connect=true

    if(this.checkingFieds(myform.value.email,myform.value.password)){
      this.errorMessage = 'Le pseudo et le mot de passe sont tous obligatoires';
        this.progress=false
        this.statusEnable=false
        this.connect=false

        return
    }
  
  let compte = {
    "email":myform.value.email,
    "password":myform.value.password
  };

  this.authService.auth(compte).subscribe(
    (reponse)=>{
      
      if( reponse!=null){
           //reussie
           this.user = reponse

          // Save in local storage
          Localstore.setCompte("COMPTE",JSON.stringify(this.user))
        
           this.progress=false
           this.statusEnable=false
           this.connect=true

           this.authService.changeLogged(true);
           this.router.navigate(['/'])

      }
      else{
        console.log("==================")
        console.log(reponse)
      /*   if(reponse.result.code === 201 || reponse.result.code === 202){
          this.errorMessage = 'Votre mot de passe ou le pseudo incorrect';
        }
        else if(reponse.result.code === 204){
          this.errorMessage = 'Désolé ! Ce compte est bloqué';
        }
 */
        this.progress=false
        this.statusEnable=false
        this.connect=false

      }
    },
    (error)=>{
      
      console.log(error)
        this.errorMessage = 'Votre mot de passe ou le pseudo incorrect. Veuillez contacter l\'administrateur';
        this.progress=false
        this.statusEnable=false
        this.connect=false
    },
  );

    }

    checkingFieds(pseudo:string, password:string): boolean{
      if(pseudo==="" || password===""){
        return true;
      }else{return false;}
    }


}
