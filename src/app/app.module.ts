import { NgModule,CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IconsProviderModule } from './icons-provider.module';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzInputModule } from 'ng-zorro-antd/input';
import { LoginComponent } from './pages/login/login.component';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';
import { AccountComponent } from './components/user/account/account.component';
import { AccountListItemComponent } from './components/user/account-list-item/account-list-item.component';
import { AccountNewComponent } from './components/user/account-new/account-new.component';
import { NgZorroAntdModul } from './ng-zorro-antd.module';
import { AppBaseComponent } from './pages/app-base/app-base.component';
import { AuthGuardService } from './guars/AuthGuardService';
import { AuthService } from './services/auth.service';
import { PriorityNewComponent } from './components/priority/priority-new/priority-new.component';
import { PriorityListItemComponent } from './components/priority/priority-list-item/priority-list-item.component';
import { PriorityComponent } from './components/priority/priority/priority.component';
import { TypeIncidentNewComponent } from './components/typeIncident/type-incident-new/type-incident-new.component';
import { TypeIncidentListItemComponent } from './components/typeIncident/type-incident-list-item/type-incident-list-item.component';
import { TypeIncidentComponent } from './components/typeIncident/type-incident/type-incident.component';
import { RequestorNewComponent } from './components/requestor/requestor-new/requestor-new.component';
import { RequestorListItemComponent } from './components/requestor/requestor-list-item/requestor-list-item.component';
import { RequestorComponent } from './components/requestor/requestor/requestor.component';
import { TicketListDoneComponent } from './components/ticket/ticket-list-done/ticket-list-done.component';
import { TicketListItemOpenComponent } from './components/ticket/ticket-list-item-open/ticket-list-item-open.component';
import { TicketNewComponent } from './components/ticket/ticket-new/ticket-new.component';
import { TicketProcessComponent } from './components/ticket/ticket-process/ticket-process.component';
import { ConfigAppComponent } from './pages/config-app/config-app.component';
import { ConfigAccountComponent } from './pages/config-account/config-account.component';
import { ProfilAccountComponent } from './components/profil-account/profil-account.component';
import { SiteNewComponent } from './components/configSystem/site/site-new/site-new.component';
import { DirectionNewComponent } from './components/configSystem/direction/direction-new/direction-new.component';


registerLocaleData(en);

@NgModule({
  declarations: [
    AppComponent,
    AppBaseComponent,
    LoginComponent,
    AccountComponent,
    AccountListItemComponent,
    AccountNewComponent,
    PriorityNewComponent,
    PriorityListItemComponent,
    PriorityComponent,
    TypeIncidentNewComponent,
    TypeIncidentListItemComponent,
    TypeIncidentComponent,
    RequestorNewComponent,
    RequestorListItemComponent,
    RequestorComponent,
    TicketProcessComponent,
    TicketNewComponent,
    TicketListItemOpenComponent,
    TicketListDoneComponent,
    ConfigAppComponent,
    ConfigAccountComponent,
    ProfilAccountComponent,
    SiteNewComponent,
    DirectionNewComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    IconsProviderModule,
    NgZorroAntdModul,
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    AuthGuardService,
    AuthService
  
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA
  ],
  
  bootstrap: [AppComponent]
})
export class AppModule { }
