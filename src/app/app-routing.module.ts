import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DirectionNewComponent } from './components/configSystem/direction/direction-new/direction-new.component';
import { SiteNewComponent } from './components/configSystem/site/site-new/site-new.component';
import { PriorityComponent } from './components/priority/priority/priority.component';
import { ProfilAccountComponent } from './components/profil-account/profil-account.component';
import { RequestorComponent } from './components/requestor/requestor/requestor.component';
import { TicketListDoneComponent } from './components/ticket/ticket-list-done/ticket-list-done.component';
import { TicketProcessComponent } from './components/ticket/ticket-process/ticket-process.component';
import { TypeIncidentComponent } from './components/typeIncident/type-incident/type-incident.component';
import { AccountComponent } from './components/user/account/account.component';
import { AuthGuardService } from './guars/AuthGuardService';
import { AppBaseComponent } from './pages/app-base/app-base.component';
import { ConfigAccountComponent } from './pages/config-account/config-account.component';
import { ConfigAppComponent } from './pages/config-app/config-app.component';
import { LoginComponent } from './pages/login/login.component';


const routes: Routes = [
  /* { path: '', pathMatch: 'full', redirectTo: '/welcome' },
  { path: 'login', component: LoginComponent },
  { path: 'welcome', loadChildren: () => import('./pages/welcome/welcome.module').then(m => m.WelcomeModule) }
 */

  { path: '', redirectTo:"home", pathMatch:'full'},
  { path: 'auth', component: LoginComponent }, 
  
  { 
    path: 'home',canActivate :[AuthGuardService] ,
    children:[
      { path: "", redirectTo:"ticket-process", pathMatch:'full'}, 
      { path: 'ticket-process',canActivate :[AuthGuardService] , component: TicketProcessComponent }, 
      { path: 'ticket-done',canActivate :[AuthGuardService] , component: TicketListDoneComponent}, 
    ]

  },

  { 
    path: 'config-system',canActivate :[AuthGuardService] , component:ConfigAppComponent ,
    children:[
      { path: "", redirectTo:"site", pathMatch:'full'},
      { path: 'site',canActivate :[AuthGuardService] , component: SiteNewComponent }, 
      { path: 'direction',canActivate :[AuthGuardService] , component: DirectionNewComponent }, 
      { path: 'type-incident',canActivate :[AuthGuardService] , component: TypeIncidentComponent }, 
      { path: 'priority',canActivate :[AuthGuardService] , component: PriorityComponent }, 
    ]

  },
  { 
    path: 'config-account',canActivate :[AuthGuardService], component: ConfigAccountComponent ,
    children:[
      { path: "", redirectTo:"account", pathMatch:'full'}, 
      { path: 'account',canActivate :[AuthGuardService] , component: AccountComponent }, 
      { path: 'profil',canActivate :[AuthGuardService] , component: ProfilAccountComponent },  
    ]

  },
  



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
