import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { Observable } from 'rxjs';
import  config from '../config.json';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

isAuth=false;

private loggedIn=new BehaviorSubject<boolean>(false)
  
constructor(private route:Router,private http:HttpClient) { }

get isLoggedIn(){
  return this.loggedIn.asObservable()
}

changeLogged(value:boolean){
  this.loggedIn.next(value)
}

logout(){
  this.loggedIn.next(false)
  this.route.navigate(['/auth'])
}

login(user:any){
  if(user.userName!=='' && user.password!=''){
    this.loggedIn.next(true)
    this.route.navigate(['/'])
  }
}

auth(compte:any):Observable <any>{
  return this.http.post<any>(config.URL_BASE+config.SUB_RESSOURCE.USER+config.V1.AUTH,compte);
}

add(user:any): Observable  <any> {
  return this.http.post<any>(config.URL_BASE+config.SUB_RESSOURCE.USER+config.V1.ADD,user);
}

}


