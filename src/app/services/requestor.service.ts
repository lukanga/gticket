import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';

import config from '../config.json';

@Injectable({
  providedIn: 'root'
})
export class RequestoreService {

  constructor(private http: HttpClient) {

  }

// http request

getAll(): Observable <any> {
  return this.http.get<any>(config.URL_BASE + config.SUB_RESSOURCE.REQUESTORE + config.V1.GET_ALL);
}

get(id:number): Observable <any> {
  return this.http.get<any>(config.URL_BASE  + config.SUB_RESSOURCE.REQUESTORE + config.V1.GET+id);
}

add(requestor:any): Observable  <any> {
  return this.http.post<any>(config.URL_BASE+config.SUB_RESSOURCE.REQUESTORE+config.V1.ADD,requestor);
}

}
