export class Ticket {

      // fieds
      public id:number=0;
      public objet:string="";
      public title_it:string="";
      public title_requestor:string="";
      public description_requestor:string="";
      public description_it:string="";
      public criticality_requestor:number=0;
      public criticality_it:number=0;
      public date_create:string="";
      public date_resolve:string="";
      public status:string="";
      public observation:string="";
    
      public id_type_incident_it:number=0;
      public id_type_incident_requestor:number=0;
      public id_priority_requestor:number=0;
      public id_priority_it:number=0;
      public id_requestor:number=0;
      public id_user_create:number=0;
      public delai_resolution_requestor:number=0;
      public delai_resolution_it:number=0;
  
      // Constructor
  
      constructor(){
        
      }
  
  
  
      // Methodes
}
