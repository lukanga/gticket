import { Component } from '@angular/core';

import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { PriorityItem } from './PriorityItem';

export interface ColumnPriorityItem {
    name: string;
    sortOrder: NzTableSortOrder | null;
    sortFn: NzTableSortFn<PriorityItem> | null;
    listOfFilter: NzTableFilterList;
    filterFn: NzTableFilterFn<PriorityItem> | null;
    filterMultiple: boolean;
    sortDirections: NzTableSortOrder[];
  }