import { Priority } from "../models/priority";
import { Requestor } from "../models/requestor";
import { TypeIncident } from "../models/typeIncident";
import { User } from "../models/user";

export interface TicketItem {
    requestor: Requestor;
    title_requestor: string;
    title_it: string;
    priority_requestor: Priority;
    type_incident_requestor: TypeIncident;
    type_incident_it: TypeIncident;
    user_create: User;
    criticality_requestor: number;
    criticality_it: number;
    date_create: string;
    description_requestor: string;
    observation: string;
    description_it: string;
    priority_it: Priority;
    objet: string;
    date_resolve: string;
    status: string;
    delai_resolution_requestor: string;
    delai_resolution_it: string;
    id: number;
  }