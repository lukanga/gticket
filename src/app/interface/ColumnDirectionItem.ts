import { Component } from '@angular/core';

import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { DirectionItem } from './DirectionItem';

export interface ColumnDirectionItem {
    name: string;
    sortOrder: NzTableSortOrder | null;
    sortFn: NzTableSortFn<DirectionItem> | null;
    listOfFilter: NzTableFilterList;
    filterFn: NzTableFilterFn<DirectionItem> | null;
    filterMultiple: boolean;
    sortDirections: NzTableSortOrder[];
  }