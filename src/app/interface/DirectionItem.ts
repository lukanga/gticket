import { Site } from "../models/site";

export interface DirectionItem {
    id: number;
    name: string;
    site: Site;
  }