import { Component } from '@angular/core';

import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { SiteItem } from './SiteItem';

export interface ColumnSiteItem {
    name: string;
    sortOrder: NzTableSortOrder | null;
    sortFn: NzTableSortFn<SiteItem> | null;
    listOfFilter: NzTableFilterList;
    filterFn: NzTableFilterFn<SiteItem> | null;
    filterMultiple: boolean;
    sortDirections: NzTableSortOrder[];
  }