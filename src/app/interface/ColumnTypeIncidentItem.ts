import { Component } from '@angular/core';

import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { TypeIncidentItem } from './TypeIncidentItem';

export interface ColumnTypeIncidentItem {
    name: string;
    sortOrder: NzTableSortOrder | null;
    sortFn: NzTableSortFn<TypeIncidentItem> | null;
    listOfFilter: NzTableFilterList;
    filterFn: NzTableFilterFn<TypeIncidentItem> | null;
    filterMultiple: boolean;
    sortDirections: NzTableSortOrder[];
  }