import { Component } from '@angular/core';

import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { TicketItem } from './TicketItem';

export interface ColumnTicketItem {
    name: string;
    sortOrder: NzTableSortOrder | null;
    sortFn: NzTableSortFn<TicketItem> | null;
    listOfFilter: NzTableFilterList;
    filterFn: NzTableFilterFn<TicketItem> | null;
    filterMultiple: boolean;
    sortDirections: NzTableSortOrder[];
  }