import { Component } from '@angular/core';

import { NzTableFilterFn, NzTableFilterList, NzTableSortFn, NzTableSortOrder } from 'ng-zorro-antd/table';
import { RequestorItem } from './RequestorItem';

export interface ColumnRequestorItem {
    name: string;
    sortOrder: NzTableSortOrder | null;
    sortFn: NzTableSortFn<RequestorItem> | null;
    listOfFilter: NzTableFilterList;
    filterFn: NzTableFilterFn<RequestorItem> | null;
    filterMultiple: boolean;
    sortDirections: NzTableSortOrder[];
  }