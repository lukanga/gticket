import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { NzModalService } from 'ng-zorro-antd/modal';
import { DataItem } from 'src/app/interface/DataItem';
import { Direction } from 'src/app/models/direction';
import { Site } from 'src/app/models/site';
import { User } from 'src/app/models/user';
import { DirectionService } from 'src/app/services/direction.service';
import { SiteService } from 'src/app/services/site.service';
import { UserService } from 'src/app/services/user.service';
import { Localstore } from 'src/app/store/local.store';

@Component({
  selector: 'app-account-new',
  templateUrl: './account-new.component.html',
  styleUrls: ['./account-new.component.css']
})
export class AccountNewComponent implements OnInit {

  @Output() newAccountEvent = new EventEmitter<DataItem>();
  validateForm!: FormGroup;
  profil_value: string = ""
  direction_value: number = 0
  site_value: number = 0
  listProfil: string[] = new Array();

  listDirections: Direction[] = []
  listDirections_tempo: Direction[] = []
  listSites: Site[] = []


  isLoadingOne = false;

  loadOne(): void {
    this.isLoadingOne = true;
  }


  constructor(private fb: FormBuilder, private serviceHttp: UserService,private servSiteHttp:SiteService,
    private servDirectionHttp:DirectionService,
    private modalService: NzModalService) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      names: [null, [Validators.required]],
      profil: [null, [Validators.required]],
      password: [null, [Validators.required]],
      email: [null, [Validators.required]],
      site: [null, [Validators.required]],
      direction: [null, [Validators.required]],
    });

    this.getProfil()
    this.getAllSites()
    this.getAllDirection()
  }


  add(myform: any,e: MouseEvent) {

  const isValid=  this.submitForm()
  if(!isValid){
    return
  }
     
    this.loadOne()
    let profil: number =0
    switch (this.profil_value) {
      case "Administrateur":
        profil=1
        break;
      case "IT":
        profil=2
        break;
      case "Simple":
        profil=3
        break;
      default:
        break;
    }
  
    let obj = {
      "names": myform.value.names,
      "profil": profil,
      "password": myform.value.password,
      "email": myform.value.email,
      "id_direction": this.direction_value
    };

    this.serviceHttp.add(obj).subscribe(
      (reponse) => {

        console.log(reponse)
        if (reponse != null) {
          //reussie

          // Save in local storage
          this.resetForm(e) // Reset form
          this.isLoadingOne = false;
          this.success()

        const newObj:DataItem=reponse;
        this.addNewItem(newObj)

        } else {

          this.isLoadingOne = false;
          this.error()
          
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {
        console.log(error)
        this.isLoadingOne = false;
        this.error()
      },
    );

  }

  success(): void {
     this.modalService.success({
      nzTitle: 'Info enregistrement',
      nzContent: "Le compte d'utilisateur a été créé avec succes et l'utilisateur en question peux se connecter sans problème."
    });
  }

  error(): void {
    this.modalService.error({
      nzTitle: 'Info enregistrement',
      nzContent: "L'enregistrement de compte a échoué. Soit un compte avec la même adresse e-mail existe déjà. Veuillez contacter l'administrateur."
    });
  }

  checkingFieds(name: string): boolean {
    if (name === "") {
      return true;
    } else { return false; }
  }

  submitForm(): boolean {
   let isValid:boolean=true;
    if (this.validateForm.valid) {
      return true
    } else {
      Object.values(this.validateForm.controls).forEach(control => {
        if (control.invalid) {
          isValid=false;
          control.markAsDirty();
          control.updateValueAndValidity({ onlySelf: true });
        }
      });
      return isValid
    }
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsPristine();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
  }


  profilChange(value: string): void {

    this.profil_value = value;
  }

  siteChange(value: string): void {
    
    this.site_value = Number(value);
    console.log(value)
    this.loadDierction()

  }
  directionChange(value: string): void {
    console.log(value)
    this.direction_value = Number(value);

  }

  loadDierction():void{
   
    this.listDirections=[]
    this.listDirections=this.listDirections_tempo.filter(item => item.site.id==this.site_value)
  
  }


  getProfil() {
    this.listProfil?.push("Administrateur")
    this.listProfil?.push("IT")
    this.listProfil?.push("Simple")
  }

   // Event emiter send data the children to parent 
   addNewItem(value: DataItem) {
    this.newAccountEvent.emit(value);
  }



  getAllSites() {

    this.servSiteHttp.getAll().subscribe(
      (reponse) => {


        if (reponse != null) {
          //reussie
          this.listSites = reponse;

          console.table(this.listSites)
          // Save in local storage


        } else {
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {
        console.log(error)

      },
    );
  }


  getAllDirection() {

    this.servDirectionHttp.getAll().subscribe(
      (reponse) => {

        console.log(reponse)
        if (reponse != null) {
          //reussie
          this.listDirections_tempo = reponse;

          console.table(this.listDirections_tempo)
          // Save in local storage


        } else {
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {
        console.log(error)

      },
    );
  }

}
