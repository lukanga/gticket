import { Component, OnInit,Input } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import {ColumnItem} from '../../../interface/ColumnItem'
import {DataItem} from '../../../interface/DataItem'


@Component({
  selector: 'app-account-list-item',
  templateUrl: './account-list-item.component.html',
  styleUrls: ['./account-list-item.component.css']
})
export class AccountListItemComponent implements OnInit {
  loading = false;
  @Input() listOfData: DataItem[] = []; // the property with @Input()
  constructor(){}
  ngOnInit(): void {
   
  }

  listOfColumns: ColumnItem[] = [
    {
      name: 'Adresse',
      sortOrder: null,
      sortFn: (a: DataItem, b: DataItem) => a.email.localeCompare(b.email),
      sortDirections: ['ascend', 'descend', null],
      filterMultiple: true,
      listOfFilter: [],
      filterFn: (list: string[], item: DataItem) => list.some(email => item.email.indexOf(email) !== -1)
    },
    {
      name: 'Identite',
      sortOrder: null,
      sortFn: (a: DataItem, b: DataItem) => a.names.length - b.names.length,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    },
    {
      name: 'Profil',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: DataItem, b: DataItem) => a.profil - b.profil,
      filterMultiple: false,
      listOfFilter: [],
      filterFn: null
    }
  ];

 

}
