import { Component, OnInit } from '@angular/core';
import { DataItem } from 'src/app/interface/DataItem';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  listOfData: DataItem[] = [];

  constructor(private serviceHttp: UserService) { }

  ngOnInit() {
    this.getAll();
  }


  getAll(){

    this.serviceHttp.getAll().subscribe(
      (reponse)=>{
        
        if( reponse!=null){
             //reussie
              this.listOfData=reponse;
             
          
        }else{
          console.log(reponse)
          if(reponse.result.code === 201 || reponse.result.code === 202){
            
          }
          else if(reponse.result.code === 204){
           
          }
  
          
        }
      },
      (error)=>{
        console.log(error)
          
      },
    );
  }

  addItem(newItem: DataItem) {
    //this.listOfData.push(newItem);
    this.getAll()
  }

}
