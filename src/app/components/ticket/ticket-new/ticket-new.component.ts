import { Component, OnInit , Output, EventEmitter} from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators, ValidationErrors } from '@angular/forms';

import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import Swal from 'sweetalert2'

import { Observable, Observer, ReplaySubject } from 'rxjs';
import { TicketItem } from 'src/app/interface/TicketItem';
import { Priority } from 'src/app/models/priority';
import { Requestor } from 'src/app/models/requestor';
import { TypeIncident } from 'src/app/models/typeIncident';
import { User } from 'src/app/models/user';
import { PriorityService } from 'src/app/services/priority.service';
import { RequestoreService } from 'src/app/services/requestor.service';
import { TicketService } from 'src/app/services/ticket.service';
import { TypeIncidentService } from 'src/app/services/type.incident.service';
import { Localstore } from 'src/app/store/local.store';
import { NzMessageService } from 'ng-zorro-antd/message';
import { NzUploadChangeParam, NzUploadFile } from 'ng-zorro-antd/upload';
@Component({
  selector: 'app-ticket-new',
  templateUrl: './ticket-new.component.html',
  styleUrls: ['./ticket-new.component.css']
})
export class TicketNewComponent implements OnInit {

  @Output() newTicketEvent = new EventEmitter<TicketItem>();

  validateForm!: FormGroup;

  requestor_value: number = 0
  type_incident_value: number = 0
  priority_value: number = 0
  criticity_value: number = 0
  delai: string = ""
  delai_value: number = 0
  description: string = ""
  nrSelect: number = 0;

  base64Output!: string;

  listRequestor: Requestor[] = []; 
  listTypeIncident: TypeIncident[] = []
  listPriority: Priority[] = []
  listDelaiResolution: string[] = new Array();


  isLoadingOne = false;

  UPLOAD_FILE = '';

  loadOne(): void {
    this.isLoadingOne = true;
  }

  constructor(private fb: FormBuilder, private serviceHttp: TicketService,
    private servRequestor: RequestoreService,
    private servTypeIncident: TypeIncidentService,
    private servPriority: PriorityService,
    private msg: NzMessageService) { }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      titre: [null, [Validators.required]],
      description: [null, [Validators.required]],
      requestor: [null, [Validators.required]],
      type_incident: [null, [Validators.required]],
      priority: [null, [Validators.required]],
      delai: [null, [Validators.required]],
    });



    let compte :User=JSON.parse(Localstore.getCompte("COMPTE"));
    const id_user:number=compte.id;
    this.nrSelect =id_user

    console.log(this.nrSelect)

    this.getAllRequestor()
    this.getAllTypeIncident()
    this.getAllPriority()
    this.getDelaiResolution()
  }

  submitForm(): boolean {
    let isValid:boolean=true;
     if (this.validateForm.valid) {
       return true
     } else {
       Object.values(this.validateForm.controls).forEach(control => {
         if (control.invalid) {
           isValid=false;
           control.markAsDirty();
           control.updateValueAndValidity({ onlySelf: true });
         }
       });
       return isValid
     }
   }

  beforeUpload = (file: NzUploadFile, _fileList: NzUploadFile[]): Observable<boolean> =>


  new Observable((observer: Observer<boolean>) => {
    console.log("starting upload image")
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
    if (!isJpgOrPng) {
      this.msg.error('Vous ne pouvez télécharger que des fichiers JPG ou png');
      observer.complete();
      return;
    }
    const isLt2M = file.size! / 1024 / 1024 < 2;
    if (!isLt2M) {
      this.msg.error("L'image doit être inférieure à 2 Mo");
      observer.complete();
      return;
    }

    observer.next(isJpgOrPng && isLt2M);
    observer.complete();
  });

  private getBase64(img: File, callback: (img: string) => void): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result!.toString()));
    reader.readAsDataURL(img);
  }

  handleChange(info: { file: NzUploadFile }): void {
    console.log("changing upload")
    switch (info.file.status) {
      case 'uploading':
       
        break;
      case 'done':
        console.log("Task finished upload image")
        // Get this url from response in real world.
        this.getBase64(info.file!.originFileObj!, (img: string) => {
          
        });
        break;
      case 'error':
        console.log("Task error upload image")
        this.msg.error('Network error');
      
        break;
    }
  }


  convertFile(file : File) : Observable<string> {
    const result = new ReplaySubject<string>(1);
    const reader = new FileReader();
    reader.readAsBinaryString(file);
    reader.onload = (event:any) => result.next(btoa(event.target.result.toString()));
    return result;
  }

  getDelaiResolution() {


    this.listDelaiResolution?.push("0.5")
    this.listDelaiResolution?.push("1")
    this.listDelaiResolution?.push("3")
    this.listDelaiResolution?.push("6")
    this.listDelaiResolution?.push("12")
    this.listDelaiResolution?.push("24")
    this.listDelaiResolution?.push("48")
    this.listDelaiResolution?.push("72")
    this.listDelaiResolution?.push("96")
    this.listDelaiResolution?.push("120")
    this.listDelaiResolution?.push("144")
    this.listDelaiResolution?.push("168")
    this.listDelaiResolution?.push("168")
    this.listDelaiResolution?.push("192")
    this.listDelaiResolution?.push("216")
    this.listDelaiResolution?.push("240")
    this.listDelaiResolution?.push("264")
    this.listDelaiResolution?.push("264")
    this.listDelaiResolution?.push("264")
    this.listDelaiResolution?.push("Indeterminée")


  }


  requestorChange(value: string): void {
    
    for(const item of this.listRequestor){
         if(item.id_user===Number(value)){
          this.requestor_value = Number(item.id);
          console.log( this.requestor_value)
          break;
         }
    }
   
  }

  // Event emiter send data the children to parent 
  addNewItem(value: TicketItem) {
    this.newTicketEvent.emit(value);
  }

  typeIncidentChange(value: string): void {
    console.log(value)
    this.type_incident_value = Number(value);
  }

  priorityChange(value: string): void {
    console.log(value)
    this.priority_value = Number(value);
  }

  delaiChange(value: string): void {
    console.log(value)
    try {
      this.delai_value =Number (value);
    } catch (error) {
      this.delai_value= -1;
    }
  }

  descriptionChange(value: string): void {
    console.log(value)
    this.description = value;
  }


  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  userNameAsyncValidator = (control: FormControl) =>
    new Observable((observer: Observer<ValidationErrors | null>) => {
      setTimeout(() => {
        if (control.value === 'JasonWood') {
          // you have to return `{error: true}` to mark it as an error event
          observer.next({ error: true, duplicated: true });
        } else {
          observer.next(null);
        }
        observer.complete();
      }, 1000);
    });

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  }

  add(myform: any,e: MouseEvent) {

    const isValid=  this.submitForm()
    if(!isValid){
      return
    }

   this.loadOne()

    let compte: User = JSON.parse(Localstore.getCompte("COMPTE"));
    const id_user = compte.id;

    /*
      "Ouvert",
      "Suspens",
      "Resolu",
      "Cloture",
    */
    let obj = {
      "objet": "Ticket",
      "id_requestor": this.requestor_value,
      "id_type_incident_requestor": this.type_incident_value,
      "title_requestor": myform.value.titre,
      "description_requestor": this.description,
      "id_priority_requestor": this.priority_value,
      "delai_resolution_requestor":Number (this.delai_value),
      "criticality_requestor": this.criticity_value,
      "status": "OUVERT",
      "id_user_create": id_user,
    };

    console.log(obj)


    this.serviceHttp.add(obj).subscribe(
      (reponse) => {

        console.log(reponse)
        if (reponse != null) {
          //reussie

          // Save in local storage

          this.resetForm(e) // Reset form
          this.isLoadingOne = false;
          this.success()

          this.criticity_value=0

          const newObj:TicketItem=reponse;
          this.addNewItem(newObj)

        } else {
          this.isLoadingOne = false;
          this.error()
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {
        this.isLoadingOne = false;
        this.error()
        console.log(error)

      },
    );

  }
  error() {
    Swal.fire({
      title: 'Info Ticket',
      text: "L'enregistrement du ticket a échoué. Contactez l'administrateur.",
      icon: 'error',
      confirmButtonText: 'Ok'
    })
  }
  success() {
    Swal.fire({
      title: 'Info Ticket',
      text: 'Le ticket a été enregistré avec succès.',
      icon: 'success',
      confirmButtonText: 'Ok'
    })

    /*

warning, error, success, info, and question
    */
  }

  checkingFieds(name: string): boolean {
    if (name === "") {
      return true;
    } else { return false; }
  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsPristine();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
  }



  getAllRequestor() {

    this.servRequestor.getAll().subscribe(
      (reponse) => {

        if (reponse != null) {
          //reussie
          this.listRequestor = reponse;

          let compte :User=JSON.parse(Localstore.getCompte("COMPTE"));
           const id_user:number=compte.id;
    

          // Save in local storage
          for(const item of this.listRequestor){
            if(item.id_user===id_user){
             this.requestor_value = Number(item.id);
             console.log( this.requestor_value)
             break;
          }
       }

        } else {
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }

        }
      },
      (error) => {
        console.log(error)

      },
    );
  }


  getAllTypeIncident() {

    this.servTypeIncident.getAll().subscribe(
      (reponse) => {


        if (reponse != null) {
          //reussie
          this.listTypeIncident = reponse;
          // Save in local storage


        } else {
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {
        console.log(error)

      },
    );
  }


  getAllPriority() {

    this.servPriority.getAll().subscribe(
      (reponse) => {

        console.log(reponse)
        if (reponse != null) {
          //reussie
          this.listPriority = reponse;
          // Save in local storage


        } else {
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {
        console.log(error)

      },
    );
  }



}
