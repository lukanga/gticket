import { Component, OnInit } from '@angular/core';
import { TicketItem } from 'src/app/interface/TicketItem';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-ticket-process',
  templateUrl: './ticket-process.component.html',
  styleUrls: ['./ticket-process.component.css']
})
export class TicketProcessComponent implements OnInit {

  listOfData: TicketItem[] = [];
  listOfDataInit: TicketItem[] = [];
  constructor(private servTicket: TicketService) { }

  ngOnInit() {
    this.getAllTicket()
  }


  getAllTicket(){

    this.servTicket.getAll().subscribe(
      (reponse)=>{
        
        console.log(reponse)
        if( reponse!=null){
             //reussie
             this.listOfDataInit=reponse;

           // let newList =  this.listOfDataInit.map(obj => obj.objet = 'Test to');

           // console.table(newList)

            this.listOfData=reponse;
            // Save in local storage
          
        }else{
          if(reponse.result.code === 201 || reponse.result.code === 202){
            
          }
          else if(reponse.result.code === 204){
           
          }
  
          
        }
      },
      (error)=>{
        console.log(error)
          
      },
    );
  }

  addItem(newItem: TicketItem) {
    console.log(newItem)
    // this.listOfData.push(newItem);
    this.getAllTicket()
  }

}
