import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { ColumnTicketItem } from 'src/app/interface/ColumnTicketItem';
import { TicketItem } from 'src/app/interface/TicketItem';
import { Priority } from 'src/app/models/priority';
import { Requestor } from 'src/app/models/requestor';
import { TypeIncident } from 'src/app/models/typeIncident';
import { PriorityService } from 'src/app/services/priority.service';
import { RequestoreService } from 'src/app/services/requestor.service';
import { TicketService } from 'src/app/services/ticket.service';
import { TypeIncidentService } from 'src/app/services/type.incident.service';

import { Observable, Observer } from 'rxjs';
import { Localstore } from 'src/app/store/local.store';
import { User } from 'src/app/models/user';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ExcelService } from 'src/app/services/excel.service';
import { jsPDF } from 'jspdf'; 
import 'jspdf-autotable'

import { getISOWeek } from 'date-fns';
import { MenuGrantedService } from 'src/app/services/menu.granted.service';

@Component({
  selector: 'app-ticket-list-done',
  templateUrl: './ticket-list-done.component.html',
  styleUrls: ['./ticket-list-done.component.css']
})
export class TicketListDoneComponent implements OnInit {

  header = [['ID', 'Demandeur', 'Titre', 'Priorité', 'Criticité', 'Date', 'Statut', 'Délai']]

  visible = false;
  visible_view = false;

  validateForm!: FormGroup;

  requestor_value:number=0
  type_incident_value:number=0

  type_incident_it:number=0
  type_incident_requestor_it:number=0

  priority_value:number=0
  priority_requestor:number=0
  priority_it:number=0

  criticity_value:number=0
  criticity_requestor:number=0
  criticity_id:number=0

  observation_value:string=""
  title_it:string=""
  delai:string=""
  delai_it:string=""
  status:string=""
  delai_value:number=0
  status_value:string=""
  description:string=""
  observation:string=""

  listRequestor:Requestor[] =[]
  listTypeIncident:TypeIncident[] =[]
  listPriority:Priority[] =[]
  listDelaiResolution:string[]= new Array();
  listStatus:string[]= new Array();


  isVisible:boolean = false;
  isOkLoading : boolean= false;

  listOfData: TicketItem[] = [];
  listOfDataInit: TicketItem[] = [];

  ticket:TicketItem | undefined;

  isLoadingOne = false;
  loading = false;

  constructor(private fb: FormBuilder,private servTicket:  TicketService,
    private servRequestor: RequestoreService,
    private servTypeIncident: TypeIncidentService,
    private servPriority: PriorityService, 
    private modalService: NzModalService,
    private excelSrv:ExcelService,
    private servMenu:MenuGrantedService) {}


  getProfilUser(){
      return this.servMenu.getProfilUser()
    }

  loadOne(): void {
    this.isLoadingOne = true;
  }

  size: 'large' | 'small' | 'default' = 'default';

  ngOnInit(): void {
    this.getAllTicket();

      this.validateForm = this.fb.group({
      title_it: [null, [Validators.required]],
      description: [null, [Validators.required]],
      type_incident: [null, [Validators.required]],
      priority: [null, [Validators.required]],
      delai: [null, [Validators.required]],
      status: [null, [Validators.required]],
      observation: [null, [Validators.required]],
    });
  }


  initialiseList(){
    this.listTypeIncident =[]
    this.listPriority =[]
    this.listDelaiResolution= new Array();
    this.listStatus= new Array();
  }

  initialiseFields(){
  
    this.title_it="",
    this.description="",
    this.criticity_value=0,
    this.observation="",
    this.type_incident_it=0,
    this.priority_it=0,
    this.delai_it="",
    this.status_value=""
  }



  open(id:number): void {

    this.initialiseList()
    this.initialiseFields();
    this.visible = true;

    this.ticket=this.listOfData.find(item=>item.id==id)

    if(this.ticket!=undefined){

      this.type_incident_it =this.ticket.type_incident_requestor.id
      this.priority_it =this.ticket.priority_requestor.id
      this.delai_it =this.ticket.delai_resolution_requestor+""

      this.criticity_value=this.ticket.criticality_requestor
      this.description=this.ticket.description_requestor
      this.title_it=this.ticket.title_requestor

      console.log( this.type_incident_it)
      console.log( this.priority_it)
      console.log( this.delai_it)
      console.log( this.title_it)
      console.log( this.description)

    }

    this.getAllTypeIncident()
    this.getAllPriority()
    this.getDelaiResolution()
    this.getStatus()
  }


  viewTicket(id:number): void {
    this.visible_view = true;

    this.ticket=this.listOfData.find(item=>item.id==id)

    if(this.ticket!=undefined){


    }

  }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsPristine();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
  }

  close(e: MouseEvent): void {
    this.visible = false;
    this.resetForm(e) // Reset form
  }
  closeView(): void {
    this.visible_view = false;
  }



  id_item:number=0;


  listOfColumns: ColumnTicketItem[] = [
    {
      name: 'Demandeur',
      sortOrder: null,
      sortFn: (a: TicketItem, b: TicketItem) => a.requestor.names.localeCompare(b.requestor.names),
      sortDirections: ['ascend', 'descend', null],
      filterMultiple: true,
      listOfFilter: [],
      filterFn: (list: string[], item: TicketItem) => list.some(demandeur => item.requestor.names.indexOf(demandeur) !== -1)
    },
    {
      name: 'Titre',
      sortOrder: null,
      sortFn: (a: TicketItem, b: TicketItem) => a.title_requestor.length - b.title_requestor.length,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    },
    {
      name: 'Priorité',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.priority_requestor.name.length - b.priority_requestor.name.length,
      filterMultiple: false,
      listOfFilter: [],
      filterFn: (priority: string, item: TicketItem) => item.priority_requestor.name.indexOf(priority) !== -1
    },
    {
      name: 'criticité',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.criticality_it - b.criticality_it,
      filterMultiple: false,
      listOfFilter: [
        { text: '1', value: 1, },
        { text: '2', value: 2 },
        { text: '3', value: 3 },
        { text: '4', value: 4 },
        { text: '5', value: 5 },
      ],
      
      filterFn: (critical: number, item: TicketItem) => item.criticality_it===critical
    },
    {
      name: 'Date',
      sortOrder: 'ascend',
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.date_create.length - b.date_create.length,
      filterMultiple: false,
      listOfFilter: [],
      filterFn: (date_create: string, item: TicketItem) => item.date_create.indexOf(date_create) !== -1
    },
    {
      name: 'Statut',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.status.length - b.status.length,
      filterMultiple: false,
      listOfFilter: [
        { text: 'Ouvert', value: 'Ouvert'},
        { text: 'Résolu', value: 'Resolu' },
        { text: 'Suspendre', value: 'Suspens' },
        { text: 'Cloturer', value: 'cloture' },
      ],
      filterFn: (statut: string, item: TicketItem)  => item.status.indexOf(statut) !== -1
    },

    {
      name: 'Delai',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.delai_resolution_requestor.length - b.delai_resolution_requestor.length,
      filterMultiple: false,
      listOfFilter: [],
      filterFn: (delai: string, item: TicketItem) => item.delai_resolution_requestor.indexOf(delai) !== -1
    },
    {
      name: 'Action',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: () => "Traiter".length - "Traiter".length,
      filterMultiple: false,
      listOfFilter: [],
      filterFn: null
    },
  ];

  public exportToFile() {
    if(this.listOfData.length>0){
      this.excelSrv.exportAsExcelFile(this.listOfData,"ticket_liste")
    }else{
      this.error("Vous ne pouvez pas exporter car aucun ticket trouvé jusque là.")
    }
    
  }


  printTicketPDF(){
    //
    const doc = new jsPDF("l", "mm", "a4");
    let title = "Liste des tickets";
    doc.text(title,130,20);
    
    doc.setFontSize(42);
    doc.setTextColor(99);
   // let date = "Date : "+moment(new Date()).utcOffset('+0100').format('YYYY-MM-DD HH:mm:ss')

(doc as any).autoTable({
  theme:'striped',
  head: this.header,
  body: this.transformBody(),
  columnStyles: { 0: { halign: 'left' } }, // Cells in first column centered and green
  margin: { top: 45, left:20,right:20,bottom:20 },
 
  })
//
var foot = "RAWSUR SA, Société Anonyme avec Conseil d’Administration, Constituée par acte notarié du 23 février 2016 Capital social : 10.000.000.000 CDF entièrement libéré, entreprise régie par la loi n° 15/005 du 17 mars 2015 portant code des assurances, agréée par l’ARCA en date du 28/03/2019 sous le numéro 12003 Siège social : 90, Bld du 30 Juin – Kinshasa/Gombe - RDC. RCCM N° CD/KIN/RCCM/16-B-8552 – N° ID.NAT. 01-620-N05790Y – N° Impôt A1603093F";
doc.setFontSize(16);
// doc.text(foot,5, (doc as any).previous + 10);
//


doc.output("dataurlnewwindow");

 
  }

 transformBody():any[]{
    let bodyArray: any[] = []; 
     bodyArray = this.listOfData.map(obj => {
      let  rObj:string[] = [];
      rObj.push(obj.id+"", obj.requestor.names, obj.title_requestor,obj.priority_requestor.name,obj.criticality_it+"",new Date(obj.date_create).toLocaleString()+"",obj.status,obj.delai_resolution_requestor+ " H ");
      return rObj;
    });

    console.log(bodyArray)

  //
  return bodyArray;

}

  getAllTicket(){

    this.listOfData=[]
    this.servTicket.getAll().subscribe(
      (reponse)=>{
        
        if( reponse!=null){
             //reussie
           // this.listOfDataInit=reponse;

            this.listOfDataInit=reponse;

           // let newList =  this.listOfDataInit.map(obj => obj.objet = 'Test to');

           // console.table(newList)

            this.listOfData=reponse;
           
           // console.log(this.listOfDataInit)

            // Save in local storage
             
        }else{
          console.log(reponse)

          if(reponse.result.code === 201 || reponse.result.code === 202){
            
          }
          else if(reponse.result.code === 204){
           
          }
  
          
        }
      },
      (error)=>{
        console.log(error)
          
      },
    );
  }

 

  showUpdateTicket(){
  this.isVisible=!this.isVisible
  }


  showModal(): void {
    this.isVisible = true;
  }

  handleOk(e: MouseEvent): void {
      this.update(e)
  }

  handleCancel(): void {
    this.isVisible = false;
  }

  getDelaiResolution() {
    
    this.listDelaiResolution?.push("0.5")
    this.listDelaiResolution?.push("1")
    this.listDelaiResolution?.push("3")
    this.listDelaiResolution?.push("6")
    this.listDelaiResolution?.push("12")
    this.listDelaiResolution?.push("24")
    this.listDelaiResolution?.push("48")
    this.listDelaiResolution?.push("72")
    this.listDelaiResolution?.push("96")
    this.listDelaiResolution?.push("120")
    this.listDelaiResolution?.push("144")
    this.listDelaiResolution?.push("168")
    this.listDelaiResolution?.push("168")
    this.listDelaiResolution?.push("192")
    this.listDelaiResolution?.push("216")
    this.listDelaiResolution?.push("240")
    this.listDelaiResolution?.push("264")
    this.listDelaiResolution?.push("Indeterminée")

   
  }
  getStatus() {
    this.listStatus?.push("Resolu")
    this.listStatus?.push("Suspens")
    this.listStatus?.push("Cloturer")
  }

  typeIncidentChange(value: string): void {
    console.log(value)
    this.type_incident_it= Number (value);
  }

  titleChange(value: string): void {
    console.log(value)
    this.title_it=  value;
  }

  observationChange(value: string): void {
    console.log(value)
    this.observation= value;
  }

  priorityChange(value: string): void {
    console.log(value)
    this.priority_it= Number (value);
  }

  delaiChange(value: string): void {
    console.log(value)
    try {
      this.delai_it= value;
    } catch (error) {
      this.delai_it= "-1";
    }
    
  }

  statusChange(value: string): void {
    console.log(value)
    this.status_value= value;
  }

  descriptionChange(value: string): void {
    console.log(value)
    this.description= value;
  }


  submitForm(): boolean {
    let isValid:boolean=true;
     if (this.validateForm.valid) {
       return true
     } else {
       Object.values(this.validateForm.controls).forEach(control => {
         if (control.invalid) {
           isValid=false;
           control.markAsDirty();
           control.updateValueAndValidity({ onlySelf: true });
         }
       });
       return isValid
     }
   }

  // eslint-disable-next-line @typescript-eslint/explicit-function-return-type
  userNameAsyncValidator = (control: FormControl) =>
    new Observable((observer: Observer<ValidationErrors | null>) => {
      setTimeout(() => {
        if (control.value === 'JasonWood') {
          // you have to return `{error: true}` to mark it as an error event
          observer.next({ error: true, duplicated: true });
        } else {
          observer.next(null);
        }
          observer.complete();
      }, 1000);
    });

  confirmValidator = (control: FormControl): { [s: string]: boolean } => {
    if (!control.value) {
      return { error: true, required: true };
    } else if (control.value !== this.validateForm.controls.password.value) {
      return { confirm: true, error: true };
    }
    return {};
  }

  get isHorizontal(): boolean {
    return this.validateForm.controls.formLayout?.value === 'horizontal';
  }

  searchData(form:any){

  const word=form.value.word_search

  this.listOfData = this.listOfDataInit.filter(item =>
  item.requestor.names.toUpperCase().includes(word.toUpperCase()) ||
  item.title_requestor.toUpperCase().includes(word.toUpperCase()) ||
  item.title_it.includes(word) ||
  item.priority_requestor.name.toUpperCase().includes(word.toUpperCase()) ||
  item.priority_it.name.includes(word)  ||
  item.date_create.toUpperCase().includes(word.toUpperCase()) ||
  item.delai_resolution_requestor==word ||
  item.status.toUpperCase().includes(word.toUpperCase()));

  console.table(this.listOfData)

  }

  update(e: MouseEvent){
    const isValid=  this.submitForm()
    if(!isValid){
      return
    }
   this.loadOne()

   let compte :User=JSON.parse(Localstore.getCompte("COMPTE"));
   console.log(compte)
   const id_user=compte.id;

   /*
     "Ouvert",
     "suspens",
     "resolu",
     "cloture",
   */
    let obj = {
      "id":this.ticket?.id,
      "title_it":this.title_it,
      "description_it":this.description,
      "criticality_it":this.criticity_value,
      "observation":this.observation,
      "id_type_incident_it":this.type_incident_it,
      "id_priority_it":this.priority_it,
      "delai_resolution_it":Number (this.delai_it),
      "status":this.status_value.toUpperCase(),
      "id_user_update": id_user,
    };

    console.log(obj)
    this.servTicket.update(obj).subscribe(
      (reponse)=>{
        
        console.log(reponse)
        if( reponse!=null){
             //reussie
            
            // Save in local storage
            this.visible = false;

            this.resetForm(e) // Reset form
            this.isLoadingOne = false;
            this.success("Les modifications du traitement de ticket a été prises en charge avec succès")

            this.getAllTicket()

            this.initialiseList()
            this.initialiseFields();
          
        }else{
          this.isLoadingOne = false;
          this.error("Le traitement de ticket a échoué. Contactez l'administrateur.")
          console.log(reponse)
          if(reponse.result.code === 201 || reponse.result.code === 202){
            
          }
          else if(reponse.result.code === 204){
           
          }
  
          
        }
      },
      (error)=>{
        console.log(error)
        this.isLoadingOne = false;
        this.error("Le traitement de ticket a échoué. Contactez l'administrateur.")
      },
    ); 
  
  }

success(content:string): void {
    this.modalService.success({
     nzTitle: 'Info Ticket',
     nzContent: content
   });
 }

 error( content:string): void {
   this.modalService.error({
     nzTitle: 'Info Ticket',
     nzContent: content
   });
 }


  reOpenTicket(){
   
    this.loadOne()

    let compte :User=JSON.parse(Localstore.getCompte("COMPTE"));
    console.log(compte)
    const id_user=compte.id;
 
    /*
      "Ouvert",
      "suspens",
      "resolu",
      "cloture",
    */
     let obj = {
       "id":this.ticket?.id,
       "title_it":this.ticket?.title_it,
       "description_it":this.ticket?.description_it,
       "criticality_it":this.ticket?.criticality_it,
       "observation":this.ticket?.observation,
       "id_type_incident_it":this.ticket?.type_incident_it.id,
       "id_priority_it":this.ticket?.priority_it.id,
       "delai_resolution_it":Number (this.ticket?.delai_resolution_it),
       "status":"OUVERT",
       "id_user_update": id_user,
     };
 
     this.servTicket.update(obj).subscribe(
       (reponse)=>{
         
         console.log(reponse)
         if( reponse!=null){
              //reussie
             
             // Save in local storage
          this.isLoadingOne = false;
          this.visible_view = false;
          this.success("Le ticket a été réouvert avec succes.")
          this.getAllTicket()
           
         }else{
          this.isLoadingOne = false;
          this.visible_view = false;
          this.error("La réouverture du ticket a échoué. Veuillez contacter l'administrateur.")
           console.log(reponse)
           if(reponse.result.code === 201 || reponse.result.code === 202){
             
           }
           else if(reponse.result.code === 204){
            
           }
   
           
         }
       },
       (error)=>{
        this.isLoadingOne = false;
        this.visible_view = false;
        this.error("La réouverture du ticket a échoué. Veuillez contacter l'administrateur.")
         console.log(error)
           
       },
     ); 
   
   }
  
      checkingFieds(name:string): boolean{
        if(name===""){
          return true;
        }else{return false;}
      }



      getAllRequestor(){

        this.servRequestor.getAll().subscribe(
          (reponse)=>{
            
            if( reponse!=null){
                 //reussie
                this.listRequestor=reponse;
                // Save in local storage
                 
              
            }else{
              console.log(reponse)
              if(reponse.result.code === 201 || reponse.result.code === 202){
                
              }
              else if(reponse.result.code === 204){
               
              }
      
              
            }
          },
          (error)=>{
            console.log(error)
              
          },
        );
      }


      getAllTypeIncident(){

        this.servTypeIncident.getAll().subscribe(
          (reponse)=>{
            
            
            if( reponse!=null){
                 //reussie
                this.listTypeIncident=reponse;
                // Save in local storage

             /*    for(const item of this.listTypeIncident){
                  if(item.id===this.ticket?.type_incident_requestor.id){
                   this.type_incident_it = Number(item.id);
                   break;
                }
              } */
              
            }else{
              console.log(reponse)
              if(reponse.result.code === 201 || reponse.result.code === 202){
                
              }
              else if(reponse.result.code === 204){
               
              }
      
              
            }
          },
          (error)=>{
            console.log(error)
              
          },
        );
      }


      getAllPriority(){

        this.servPriority.getAll().subscribe(
          (reponse)=>{
            
            console.log(reponse)
            if( reponse!=null){
                 //reussie
                this.listPriority=reponse;
                // Save in local storage

/* 
                for(const item of this.listPriority){
                  if(item.id===this.ticket?.priority_requestor.id){
                   this.priority_it = Number(item.id);
                   break;
                }
              } */
                 
              
            }else{
              console.log(reponse)
              if(reponse.result.code === 201 || reponse.result.code === 202){
                
              }
              else if(reponse.result.code === 204){
               
              }
      
              
            }
          },
          (error)=>{
            console.log(error)
              
          },
        );
      }

}
