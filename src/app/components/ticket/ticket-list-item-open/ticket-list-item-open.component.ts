import { Component, OnInit,Input } from '@angular/core';
import { FormBuilder, FormGroup,Validators } from '@angular/forms';
import { ColumnTicketItem } from 'src/app/interface/ColumnTicketItem';
import { TicketItem } from 'src/app/interface/TicketItem';
import { TicketService } from 'src/app/services/ticket.service';

@Component({
  selector: 'app-ticket-list-item-open',
  templateUrl: './ticket-list-item-open.component.html',
  styleUrls: ['./ticket-list-item-open.component.css']
})
export class TicketListItemOpenComponent implements OnInit {
  loading = false;
  validateForm!: FormGroup;
  @Input() listOfData: TicketItem[] = []; // the property with @Input()

  constructor(private fb: FormBuilder) {}
  ngOnInit(): void {
    this.validateForm = this.fb.group({
      word_search: [null, [Validators.required]],
    });
  }


  listOfColumns: ColumnTicketItem[] = [
    {
      name: 'Demandeur',
      sortOrder: null,
      sortFn: (a: TicketItem, b: TicketItem) => a.requestor.names.localeCompare(b.requestor.names),
      sortDirections: ['ascend', 'descend', null],
      filterMultiple: true,
      listOfFilter: [],
      filterFn: (list: string[], item: TicketItem) => list.some(demandeur => item.requestor.names.indexOf(demandeur) !== -1)
    },
    {
      name: 'Titre',
      sortOrder: null,
      sortFn: (a: TicketItem, b: TicketItem) => a.title_requestor.length - b.title_requestor.length,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    },
    {
      name: 'Priorité',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.priority_requestor.name.length - b.priority_requestor.name.length,
      filterMultiple: false,
      listOfFilter: [],
      filterFn: (priority: string, item: TicketItem) => item.priority_requestor.name.indexOf(priority) !== -1
    },
    {
      name: 'criticité',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.criticality_it - b.criticality_it,
      filterMultiple: false,
      listOfFilter: [
        { text: '1', value: 1, },
        { text: '2', value: 2 },
        { text: '3', value: 3 },
        { text: '4', value: 4 },
        { text: '5', value: 5 },
      ],
      
      filterFn: (critical: number, item: TicketItem) => item.criticality_it===critical
    },
    {
      name: 'Date',
      sortOrder: 'ascend',
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.date_create.length - b.date_create.length,
      filterMultiple: false,
      listOfFilter: [],
      filterFn: (date_create: string, item: TicketItem) => item.date_create.indexOf(date_create) !== -1
    },
    {
      name: 'Statut',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.status.length - b.status.length,
      filterMultiple: false,
      listOfFilter: [
        { text: 'Ouvert', value: 'Ouvert'},
        { text: 'Résolu', value: 'Resolu' },
        { text: 'Suspendre', value: 'Suspens' },
        { text: 'Cloturer', value: 'cloture' },
      ],
      filterFn: (statut: string, item: TicketItem)  => item.status.indexOf(statut) !== -1
    },
    {
      name: 'Delai',
      sortOrder: null,
      sortDirections: ['ascend', 'descend', null],
      sortFn: (a: TicketItem, b: TicketItem) => a.delai_resolution_requestor.length - b.delai_resolution_requestor.length,
      filterMultiple: false,
      listOfFilter: [],
      filterFn: (delai: string, item: TicketItem) => item.delai_resolution_requestor.indexOf(delai) !== -1
    },
   
  ];




}
