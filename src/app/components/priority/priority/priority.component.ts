import { Component, OnInit } from '@angular/core';
import { PriorityItem } from 'src/app/interface/PriorityItem';
import { PriorityService } from 'src/app/services/priority.service';

@Component({
  selector: 'app-priority',
  templateUrl: './priority.component.html',
  styleUrls: ['./priority.component.css']
})
export class PriorityComponent implements OnInit {

  listOfData: PriorityItem[] = [];
  constructor(private httpserv: PriorityService) { }

  ngOnInit() {
    this.getAll()
  }


  getAll(){

    this.httpserv.getAll().subscribe(
      (reponse)=>{
        
        if( reponse!=null){
             //reussie
            this.listOfData=reponse;
            // Save in local storage
             
          
        }else{
          console.log(reponse)
          if(reponse.result.code === 201 || reponse.result.code === 202){
            
          }
          else if(reponse.result.code === 204){
           
          }
  
          
        }
      },
      (error)=>{
        console.log(error)
          
      },
    );
  }

  addItem(newItem: PriorityItem) {
    console.log(newItem)
    // this.listOfData.push(newItem);
    this.getAll()
  }

}
