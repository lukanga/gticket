import { Component, OnInit,Input } from '@angular/core';
import { ColumnPriorityItem } from 'src/app/interface/ColumnPriorityItem';
import { PriorityItem } from 'src/app/interface/PriorityItem';
import { PriorityService } from 'src/app/services/priority.service';

@Component({
  selector: 'app-priority-list-item',
  templateUrl: './priority-list-item.component.html',
  styleUrls: ['./priority-list-item.component.css']
})
export class PriorityListItemComponent implements OnInit {
  loading = false;
  @Input() listOfData: PriorityItem[] = [];
  constructor() {}

  ngOnInit() {
  
  }


  listOfColumns: ColumnPriorityItem[] = [
  
    {
      name: 'Priotités',
      sortOrder: 'descend',
      sortFn: (a: PriorityItem, b: PriorityItem) => a.name.length - b.name.length,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    },
   
  ];



}
