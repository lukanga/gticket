import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { NzModalService } from 'ng-zorro-antd/modal';
import { PriorityItem } from 'src/app/interface/PriorityItem';
import { PriorityService } from 'src/app/services/priority.service';
@Component({
  selector: 'app-priority-new',
  templateUrl: './priority-new.component.html',
  styleUrls: ['./priority-new.component.css']
})
export class PriorityNewComponent implements OnInit {

  validateForm!: FormGroup;
  @Output() newPriorityEvent = new EventEmitter<PriorityItem>();


  constructor(private fb: FormBuilder, private serviceHttp: PriorityService, private modalService: NzModalService) { }
  isLoadingOne = false;

  loadOne(): void {
    this.isLoadingOne = true;
  }


  ngOnInit(): void {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],

    });
  }

  submitForm(): boolean {
    let isValid:boolean=true;
     if (this.validateForm.valid) {
       return true
     } else {
       Object.values(this.validateForm.controls).forEach(control => {
         if (control.invalid) {
           isValid=false;
           control.markAsDirty();
           control.updateValueAndValidity({ onlySelf: true });
         }
       });
       return isValid
     }
   }

success(): void {
    this.modalService.success({
     nzTitle: 'Info enregistrement',
     nzContent: "La priorité a été créée avec succes."
   });
 }

 error(): void {
   this.modalService.error({
     nzTitle: 'Info enregistrement',
     nzContent: "L'enregistrement a échoué. Veuillez contacter l'administrateur."
   });
 }
  add(myform: any,e: MouseEvent) {

    const isValid=  this.submitForm()
    if(!isValid){
      return
    }

   this.loadOne()

    let obj = {
      "name": myform.value.name,
    };

    this.serviceHttp.add(obj).subscribe(
      (reponse) => {
        console.log(reponse)
        if (reponse != null) {
          //reussie

          this.resetForm(e) // Reset form
          this.isLoadingOne = false;
          this.success()
          // Save in local storage
          const newObj:PriorityItem=reponse;
          this.addNewItem(newObj)

        }
        else {
          this.isLoadingOne = false;
          this.error()
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }



        }
      },
      (error) => {
        this.isLoadingOne = false;
        this.error()
        console.log(error)

      },
    );

  }

  checkingFieds(name: string): boolean {
    if (name === "") {
      return true;
    } else { return false; }
  }



  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsPristine();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
  }

   // Event emiter send data the children to parent 
   addNewItem(value: PriorityItem) {
    this.newPriorityEvent.emit(value);
  }
}
