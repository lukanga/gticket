import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ColumnSiteItem } from 'src/app/interface/ColumnSiteItem';
import { SiteItem } from 'src/app/interface/SiteItem';
import { User } from 'src/app/models/user';
import { SiteService } from 'src/app/services/site.service';
import { Localstore } from 'src/app/store/local.store';

@Component({
  selector: 'app-site-new',
  templateUrl: './site-new.component.html',
  styleUrls: ['./site-new.component.css']
})
export class SiteNewComponent implements OnInit {
  validateForm!: FormGroup;
  listOfData: SiteItem[] = [];
  loading = true;
  constructor(private fb: FormBuilder,private servHttp: SiteService,private modalService: NzModalService) { }

  isLoadingOne = false;

  loadOne(): void {
    this.isLoadingOne = true;
  }
  ngOnInit() {

    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      adress: [null, ],
    });

  this.getAll();
  }

  submitForm(): boolean {
    let isValid:boolean=true;
     if (this.validateForm.valid) {
       return true
     } else {
       Object.values(this.validateForm.controls).forEach(control => {
         if (control.invalid) {
           isValid=false;
           control.markAsDirty();
           control.updateValueAndValidity({ onlySelf: true });
         }
       });
       return isValid
     }
   }

   success(): void {
    this.modalService.success({
     nzTitle: 'Info enregistrement',
     nzContent: "Le site a été créé avec succes."
   });
 }

 error(): void {
   this.modalService.error({
     nzTitle: 'Info enregistrement',
     nzContent: "L'enregistrement du site a échoué. Veuillez contacter l'administrateur."
   });
 }

  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsPristine();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
  }


  add(myform: any,e: MouseEvent) {

    const isValid=  this.submitForm()
    if(!isValid){
      return
    }
   this.loadOne()

    let compte: User = JSON.parse(Localstore.getCompte("COMPTE"));
    console.log(compte)
    const id_user = compte.id;

    let obj = {
      "name": myform.value.name,
      "adress": myform.value.adress,
    };


    this.servHttp.add(obj).subscribe(
      (reponse) => {

        console.log(reponse)
        if (reponse != null) {
          //reussie

          // Save in local storage
          this.resetForm(e) // Reset form
          this.isLoadingOne = false;
          this.success()


          this.getAll()

        } else {
          this.isLoadingOne = false;
          this.error()
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {
        console.log(error)
        this.isLoadingOne = false;
        this.error()

      },
    );

  }


  getAll(){

    this.listOfData=[]
    this.servHttp.getAll().subscribe(
      (reponse)=>{
        
        if( reponse!=null){
             //reussie
            this.listOfData=reponse;
            // Save in local storage
            this.loading = false;
          
        }else{
          this.loading = false;
          console.log(reponse)
          if(reponse.result.code === 201 || reponse.result.code === 202){
            
          }else if(reponse.result.code === 204){
           
          }
        }
      },
      (error)=>{
        this.loading = false;
        console.log(error)
          
      },
    );
  }


  listOfColumns: ColumnSiteItem[] = [
    {
      name: 'Site',
      sortOrder: null,
      sortFn: (a: SiteItem, b: SiteItem) => a.name.length - b.name.length,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    },
   
    
  ];


}
