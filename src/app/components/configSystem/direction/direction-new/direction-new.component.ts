import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ColumnDirectionItem } from 'src/app/interface/ColumnDirectionItem';
import { DirectionItem } from 'src/app/interface/DirectionItem';
import { Site } from 'src/app/models/site';
import { User } from 'src/app/models/user';
import { DirectionService } from 'src/app/services/direction.service';
import { SiteService } from 'src/app/services/site.service';
import { Localstore } from 'src/app/store/local.store';

@Component({
  selector: 'app-direction-new',
  templateUrl: './direction-new.component.html',
  styleUrls: ['./direction-new.component.css']
})
export class DirectionNewComponent implements OnInit {

  validateForm!: FormGroup;
  listOfData: DirectionItem[] = [];
  site_value: number = 0

  listSite: Site[] = []; 

  nrSelect: number = 0;
  loading = true;

  constructor(private fb: FormBuilder,private servSiteHttp: SiteService,private servHttp:DirectionService,private modalService: NzModalService) { }

  isLoadingOne = false;

  loadOne(): void {
    this.isLoadingOne = true;
  }
  ngOnInit() {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
      site: [null, [Validators.required]],
    });

  this.getAll();
  this.getAllSite();
  }

  success(): void {
    this.modalService.success({
     nzTitle: 'Info enregistrement',
     nzContent: "La direction a été créée avec succes."
   });
 }

 error(): void {
   this.modalService.error({
     nzTitle: 'Info enregistrement',
     nzContent: "L'enregistrement a échoué. Veuillez contacter l'administrateur."
   });
 }
  submitForm(): boolean {
    let isValid:boolean=true;
     if (this.validateForm.valid) {
       return true
     } else {
       Object.values(this.validateForm.controls).forEach(control => {
         if (control.invalid) {
           isValid=false;
           control.markAsDirty();
           control.updateValueAndValidity({ onlySelf: true });
         }
       });
       return isValid
     }
   }
  siteChange(value: string): void {
    console.log(value)
    this.site_value = Number(value);
  }


  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsPristine();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
  }


  add(myform: any,e: MouseEvent) {

    const isValid=  this.submitForm()
    if(!isValid){
      return
    }

    this.loadOne()

    let compte: User = JSON.parse(Localstore.getCompte("COMPTE"));
    console.log(compte)
    const id_user = compte.id;

    let obj = {
      "name": myform.value.name,
      "id_site": this.site_value
    };


    this.servHttp.add(obj).subscribe(
      (reponse) => {

        console.log(reponse)
        if (reponse != null) {
          //reussie

          // Save in local storage
          this.resetForm(e) // Reset form
          this.isLoadingOne = false;
          this.success()


          this.getAll()

        } else {

          this.isLoadingOne = false;
          this.error()

          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {

        this.isLoadingOne = false;
        this.error()
        console.log(error)

      },
    );

  }

  getAll(){
    this.listOfData=[]
    this.servHttp.getAll().subscribe(
      (reponse)=>{
        
        if( reponse!=null){
             //reussie
            this.listOfData=reponse;
            // Save in local storage
             this.loading = false;
          
        }else{
          this.loading = false;
          console.log(reponse)
          if(reponse.result.code === 201 || reponse.result.code === 202){
            
          }
          else if(reponse.result.code === 204){
           
          }
  
          
        }
      },
      (error)=>{
        this.loading = false;
        console.log(error)
          
      },
    );
  }
 
  getAllSite() {

    this.servSiteHttp.getAll().subscribe(
      (reponse) => {

        console.log(reponse)
        if (reponse != null) {
          //reussie
          this.listSite = reponse;
          // Save in local storage


        } else {
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {
        console.log(error)

      },
    );
  }


  listOfColumns: ColumnDirectionItem[] = [
    {
      name: 'Direction',
      sortOrder:null,
      sortFn: (a: DirectionItem, b: DirectionItem) => a.name.length - b.name.length,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    }, 
    
    {
      name: 'Site',
      sortOrder: null,
      sortFn: (a: DirectionItem, b: DirectionItem) => a.site.name.length - b.site.name.length,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    },
   
    
  ];
}
