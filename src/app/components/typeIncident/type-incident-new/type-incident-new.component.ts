import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { NzModalService } from 'ng-zorro-antd/modal';
import { TypeIncidentItem } from 'src/app/interface/TypeIncidentItem';
import { TypeIncidentService } from 'src/app/services/type.incident.service';

@Component({
  selector: 'app-type-incident-new',
  templateUrl: './type-incident-new.component.html',
  styleUrls: ['./type-incident-new.component.css']
})
export class TypeIncidentNewComponent implements OnInit {

  validateForm!: FormGroup;

  @Output() newTypeIncidentEvent = new EventEmitter<TypeIncidentItem>();

  constructor(private fb: FormBuilder, private serviceHttp: TypeIncidentService, private modalService: NzModalService) { }

  isLoadingOne = false;

  loadOne(): void {
    this.isLoadingOne = true;
  }

  ngOnInit(): void {
    this.validateForm = this.fb.group({
      name: [null, [Validators.required]],
    });
  }

  submitForm(): boolean {
    let isValid:boolean=true;
     if (this.validateForm.valid) {
       return true
     } else {
       Object.values(this.validateForm.controls).forEach(control => {
         if (control.invalid) {
           isValid=false;
           control.markAsDirty();
           control.updateValueAndValidity({ onlySelf: true });
         }
       });
       return isValid
     }
   }

success(): void {
    this.modalService.success({
     nzTitle: 'Info enregistrement',
     nzContent: "Le type d'incident a été créé avec succes."
   });
 }

 error(): void {
   this.modalService.error({
     nzTitle: 'Info enregistrement',
     nzContent: "L'enregistrement a échoué. Veuillez contacter l'administrateur."
   });
 }

  add(myform: any,e: MouseEvent) {

    const isValid=  this.submitForm()
    if(!isValid){
      return
    }

   this.loadOne()

    let obj = {
      "name": myform.value.name,
    };

    this.serviceHttp.add(obj).subscribe(
      (reponse) => {

        
        if (reponse != null) {
          //reussie
        const newObj:TypeIncidentItem=reponse;
        this.addNewItem(newObj)
         
          // Save in local storage

          this.resetForm(e) // Reset form
          this.isLoadingOne = false;
          this.success()
        }
        else {

          this.isLoadingOne = false;
          this.error()
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }



        }
      },
      (error) => {
        console.log(error)
        this.isLoadingOne = false;
        this.error()
      },
    );

  }

  checkingFieds(name: string): boolean {
    if (name === "") {
      return true;
    } else { return false; }
  }


  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsPristine();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
  }

 // Event emiter send data the children to parent 
 addNewItem(value: TypeIncidentItem) {
  this.newTypeIncidentEvent.emit(value);
}


}
