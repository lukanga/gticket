import { Component, OnInit } from '@angular/core';
import { TypeIncidentItem } from 'src/app/interface/TypeIncidentItem';
import { TypeIncidentService } from 'src/app/services/type.incident.service';

@Component({
  selector: 'app-type-incident',
  templateUrl: './type-incident.component.html',
  styleUrls: ['./type-incident.component.css']
})
export class TypeIncidentComponent implements OnInit {

  listOfData: TypeIncidentItem[] = [];
  constructor(private httpserv: TypeIncidentService) { }

  ngOnInit() {
    this.getAll();
  }


  getAll(){

    this.httpserv.getAll().subscribe(
      (reponse)=>{
        
        if( reponse!=null){
             //reussie
            this.listOfData=reponse;
            // Save in local storage
             
          
        }else{
          console.log(reponse)
          if(reponse.result.code === 201 || reponse.result.code === 202){
            
          }
          else if(reponse.result.code === 204){
           
          }
  
          
        }
      },
      (error)=>{
        console.log(error)
          
      },
    );
  }


  addItem(newItem: TypeIncidentItem) {
    console.log(newItem)
    // this.listOfData.push(newItem);
    this.getAll()
  }

}
