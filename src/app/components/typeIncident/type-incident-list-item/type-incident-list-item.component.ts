import { Component, OnInit,Input } from '@angular/core';
import { ColumnTypeIncidentItem } from 'src/app/interface/ColumnTypeIncidentItem';
import { TypeIncidentItem } from 'src/app/interface/TypeIncidentItem';
import { TypeIncidentService } from 'src/app/services/type.incident.service';

@Component({
  selector: 'app-type-incident-list-item',
  templateUrl: './type-incident-list-item.component.html',
  styleUrls: ['./type-incident-list-item.component.css']
})
export class TypeIncidentListItemComponent implements OnInit {
  loading = false;
@Input()  listOfData: TypeIncidentItem[] = [];

  constructor() {}

  ngOnInit() {
   
  }


  listOfColumns: ColumnTypeIncidentItem[] = [


  
    {
      name: 'Type Incident',
      sortOrder: 'descend',
      sortFn: (a: TypeIncidentItem, b: TypeIncidentItem) => a.name.length - b.name.length,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    },
  ];

 

}
