import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-update-requestor',
  templateUrl: './update-requestor.component.html',
  styleUrls: ['./update-requestor.component.css']
})
export class UpdateRequestorComponent implements OnInit {
  
  isVisible:boolean = false;
  isOkLoading : boolean= false;

  constructor() { }
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }


  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    this.isOkLoading = true;
    setTimeout(() => {
      this.isVisible = false;
      this.isOkLoading = false;
    }, 3000);
  }

  handleCancel(): void {
    this.isVisible = false;
  }
}
