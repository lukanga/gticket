import { Component, OnInit,Input } from '@angular/core';
import { ColumnRequestorItem } from 'src/app/interface/ColumnRequestorItem';
import { RequestorItem } from 'src/app/interface/RequestorItem';
import { RequestoreService } from 'src/app/services/requestor.service';

@Component({
  selector: 'app-requestor-list-item',
  templateUrl: './requestor-list-item.component.html',
  styleUrls: ['./requestor-list-item.component.css']
})
export class RequestorListItemComponent implements OnInit {
  @Input() listOfData: RequestorItem[] = [];
  constructor() {}

  ngOnInit() {
 
  }

  listOfColumns: ColumnRequestorItem[] = [
    {
      name: 'Id',
      sortOrder: 'descend',
      sortFn: (a: RequestorItem, b: RequestorItem) => a.id - b.id,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    },
  
    {
      name: 'Nom complet demandeur',
      sortOrder: 'descend',
      sortFn: (a: RequestorItem, b: RequestorItem) => a.names.length - b.names.length,
      sortDirections: ['descend', null],
      listOfFilter: [],
      filterFn: null,
      filterMultiple: true
    },
   
    
  ];

 

}
