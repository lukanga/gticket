import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NzFormTooltipIcon } from 'ng-zorro-antd/form';
import { RequestorItem } from 'src/app/interface/RequestorItem';
import { User } from 'src/app/models/user';
import { RequestoreService } from 'src/app/services/requestor.service';
import { Localstore } from 'src/app/store/local.store';

@Component({
  selector: 'app-requestor-new',
  templateUrl: './requestor-new.component.html',
  styleUrls: ['./requestor-new.component.css']
})
export class RequestorNewComponent implements OnInit {

  @Output() newRequestorEvent = new EventEmitter<RequestorItem>();
  validateForm!: FormGroup;
  constructor(private fb: FormBuilder, private serviceHttp: RequestoreService) { }

  add(myform: any,e: MouseEvent) {

    let compte: User = JSON.parse(Localstore.getCompte("COMPTE"));
    console.log(compte)
    const id_user = compte.id;

    let obj = {
      "names": myform.value.names,
      "id_user": id_user,
    };


    this.serviceHttp.add(obj).subscribe(
      (reponse) => {

        console.log(reponse)
        if (reponse != null) {
          //reussie

          // Save in local storage
          this.resetForm(e) // Reset form

          const newObj:RequestorItem=reponse;
          this.addNewItem(newObj)

        } else {
          console.log(reponse)
          if (reponse.result.code === 201 || reponse.result.code === 202) {

          }
          else if (reponse.result.code === 204) {

          }


        }
      },
      (error) => {
        console.log(error)

      },
    );

  }

  checkingFieds(name: string): boolean {
    if (name === "") {
      return true;
    } else { return false; }
  }

  // Event emiter send data the children to parent 
  addNewItem(value: RequestorItem) {
    this.newRequestorEvent.emit(value);
  }

  


  ngOnInit(): void {
    this.validateForm = this.fb.group({
      names: [null, [Validators.required]],
    });
  }


  resetForm(e: MouseEvent): void {
    e.preventDefault();
    this.validateForm.reset();
    for (const key in this.validateForm.controls) {
      if (this.validateForm.controls.hasOwnProperty(key)) {
        this.validateForm.controls[key].markAsPristine();
        this.validateForm.controls[key].updateValueAndValidity();
      }
    }
  }

  
}
