import { Component, OnInit } from '@angular/core';
import { RequestorItem } from 'src/app/interface/RequestorItem';
import { RequestoreService } from 'src/app/services/requestor.service';

@Component({
  selector: 'app-requestor',
  templateUrl: './requestor.component.html',
  styleUrls: ['./requestor.component.css']
})
export class RequestorComponent implements OnInit {
  listOfData: RequestorItem[] = [];
  constructor(private servRequestor: RequestoreService) { }

  ngOnInit() {
  this.getAll();
  }


  getAll(){

    this.servRequestor.getAll().subscribe(
      (reponse)=>{
        
        if( reponse!=null){
             //reussie
            this.listOfData=reponse;
            // Save in local storage
             
          
        }else{
          console.log(reponse)
          if(reponse.result.code === 201 || reponse.result.code === 202){
            
          }
          else if(reponse.result.code === 204){
           
          }
  
          
        }
      },
      (error)=>{
        console.log(error)
          
      },
    );
  }

  addItem(newItem: RequestorItem) {
    console.log(newItem)
    // this.listOfData.push(newItem);
    this.getAll()
  }

}
